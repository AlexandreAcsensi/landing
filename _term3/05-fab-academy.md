---
title: Fab Academy
layout: "page"
order: 5
---


![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/e00c54508202-Screen_Shot_2015_02_16_at_12.54.58_PM.png)

### Faculty
Santi Fuentemilla  
Xavi Dominguez  
Eduardo Chamorro  
Esteban Gimenez  

### Syllabus and Learning Objectives
The Fab Academy is a distributed educational model directed by Neil Gershenfeld of MIT’s Center For Bits and Atoms and based on MIT’s rapid prototyping course, MAS 863: How to Make (Almost) Anything. The Fab Academy began as an outreach project from the CBA, and has since spread to Fab Labs around the world. The program provides advanced digital fabrication instruction for students through an unique, hands-on curriculum and access to technological tools and resources.

During this 6-month programme, students learn how to envision, prototype and document their projects and ideas through many hours of hands-on experience with digital fabrication tools, taking a variety of code formats and turning them into physical objects.  

### Total Duration
Students view and participate in global lectures broadcasted every Wednesdays at 9:00 am – 12:00 pm EST. The lectures are recorded and available to students throughout the semester.  
In addition to the lectures, there are 2 / 3 lab days each week where students have access the digital fabrication equipment and personal help with projects. Each Fab Lab will establish the schedule for these Lab days.  

### Structure and Phases
Apr 24: networking and communications  
Apr 29 recitation: economy  
May 01: mechanical design  
May 08: interface and application programming  
May 13 recitation: classes  
May 15: machine design  
May 22: wildcard week  
May 27 recitation: events  
May 29: invention, intellectual property, and income  
Jun 05: project development  
Jun 12: project presentations  
Jun 14: project presentations  
Jun 17: project presentations (evening)  
Jun 19: project presentations  
Global lectures happen on Wednesdays at 9:00 on the US East Coast (ranging from 6:00 on the West Coast to 23:00 in Japan). Recitations happen on Mondays at the same hour, and global lab sections and regional reviews are scheduled throughout the week.  

### Output
Each student builds a portfolio that documents their mastery of different certificates taken individually and their integration into a final, larger project. The Fab Diploma is awarded by the Fab Academy. The Fab Diploma is earned by progress rather than the calendar, for successful completion of a series of certificate requirements. The instructional sequence requires six months to cover, although the time to finish can ranged from that up to a few years.  

### Grading Method
These are reviewed by their local instructors, regional gurus, and then centrally to ensure that each student meets global standards and follows evolving best practices.  

### Background Research Material
[Fab Academy](http://fabacademy.org/)  
[Fab Foundation](http://www.fabfoundation.org/)   
[Fab Academy BCN](http://fab.academany.org/2018/labs/barcelona/local/general/)   

### SANTI FUENTIMILLA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/3857bdcbd1cd-santi.jpg)

### Email Address
<santi@fablabbcn.org>  

### Twitter Account
@santifu  

### Professional BIO
Santiago Fuentemilla Garriga (male), with Master of Architecture from the University of la Salle Universitat Ramon LLull , Spain, as a specialist in Architectural Design and Construction. In 2012 he graduated from the Fab Academy Diploma at FabLab BCN, a digital fabrication and rapid prototyping course directed by Neil Gershenfeld at MIT ́s Center For Bits and Atoms (CBA). Currently, he is undertaking a PHD in digital fabrication processes at the EGA UPC (Universitat Politecnica de Catalunya). As a professional Santi has worked in various architectural firms carrying out projects at the international level in the last 10 years. He is currently the design director at OPR (Other people’s Rooms) in Barcelona, a multidisciplinary studio based on architectural concept design for enhanced user experiences. 
Since 2013 he is part of the [Fab Lab BCN](https://fablabbcn.org) team, he is the coordinator of the Future Learning Unit (FLU), the unit focused on the design, implementation and coordination of active learning experiences with digital manufacturing tools for the community. [FLU](https://twitter.com/futurelearningu) designs and promotes educational, innovation and entrepreneurship projects such as [AmbMakers](https://twitter.com/hashtag/ambmakers) [POPUPLAB "Digital Fabrication Everywhere"](https://fablabbcn.org/popup_fab_lab.html), [FABKIDS](http://kids.fablabbcn.org/), [CROCOPOI](https://crocopoi.com). FLU participates in European research projects such as [DOIT](https://www.doit-europe.net/) or [DSISCALE](https://digitalsocial.eu/about-the-project) and [PHALABS 4.0](http://www.phablabs.eu).
Since 2014 he is Fab Instructor of the global academic program [Fab Academy](http://fabacademy.org/) and since 2017 he is professor of the Master in Design for Emergent Futures [MDEF](https://iaac.net/educational-programmes/master-design-emergent-futures) organized by IAAC.